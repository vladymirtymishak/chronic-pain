//
//  Notifications.h
//  ChronicPain
//
//  Created by Yoni on 21/09/2016.
//  Copyright © 2016 BGU. All rights reserved.
//

@import Foundation;

#import "NSDate+DayPeriod.h"

extern NSString const *kSnoozableNotificationCategoryIdentifier;
extern NSString const *kReminderNotificationCategoryIdentifier;
extern NSString const *kNotificationSnoozeActionIdentifier;
extern NSString const *kReminderNotificationFireDate;

// Note, by changing the constants below you must keep in mind that iOS allows 64 only scheduled local notifications per application.
extern NSInteger const kFurtherNotificationsDaysCount;
extern NSInteger const kNotificationRemindersCount;

/**
 This service is doing management of notifications for patient and reminds him to send data about his feelings.
 */
@interface Notifications : NSObject

+ (void)registerApplication:(UIApplication *)application;

+ (void)cancelAll;

+ (void)cancelNotificationsForCategory:(NSString *)category;

+ (void)scheduleSnoozableNotificationForDays:(NSInteger)days startingFromDate:(NSDate *)date withRemindersCount:(NSInteger)remindersCount;

+ (void)scheduleReminderNotificationAfterMinutes:(NSTimeInterval)minutes;

/**
 Restores the notification that reminds user to return to the app after taking drugs.

 @return Fire date of reminder notification
 */
+ (NSDate *)restoreReminderNotification;

@end
