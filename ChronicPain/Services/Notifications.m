//
//  Notifications.m
//  ChronicPain
//
//  Created by Yoni on 21/09/2016.
//  Copyright © 2016 BGU. All rights reserved.
//

#import "Notifications.h"

NSString const *kSnoozableNotificationCategoryIdentifier = @"SnoozableNotificationCategoryIdentifier";
NSString const *kReminderNotificationCategoryIdentifier = @"ReminderNotificationCategoryIdentifier";
NSString const *kNotificationSnoozeActionIdentifier = @"NotificationSnoozeActionIdentifier";
NSString const *kReminderNotificationFireDate = @"ReminderNotificationFireDate";

NSInteger const kFurtherNotificationsDaysCount = 2;
NSInteger const kNotificationRemindersCount = 5;

@implementation Notifications

#pragma mark - Public

+ (void)registerApplication:(UIApplication *)application {
    UIMutableUserNotificationCategory *snoozableCategory = [[UIMutableUserNotificationCategory alloc] init];
    snoozableCategory.identifier = (NSString *)kSnoozableNotificationCategoryIdentifier;
    
    UIMutableUserNotificationCategory *reminderCategory = [[UIMutableUserNotificationCategory alloc] init];
    reminderCategory.identifier = (NSString *)kReminderNotificationCategoryIdentifier;
    
    NSSet<UIUserNotificationCategory *> *categories = [NSSet setWithObjects:snoozableCategory, reminderCategory, nil];
    
    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:
                                                        UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

+ (void)cancelAll {
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

+ (void)cancelNotificationsForCategory:(NSString *)category {
    NSArray<UILocalNotification *> *scheduledNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (UILocalNotification *notification in scheduledNotifications) {
        if ([notification.category isEqualToString:category]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
}

+ (void)scheduleSnoozableNotificationForDays:(NSInteger)days startingFromDate:(NSDate *)date withRemindersCount:(NSInteger)remindersCount {
    DayPeriod dayPeriod = date.dayPeriod;
    
    if (dayPeriod == DayPeriodMorning) {
        NSDate *morningFireDate = [date randomizeForDayPeriod:dayPeriod];
        UILocalNotification *morningNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:morningFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:morningNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:morningFireDate];
        
        NSDate *noonFireDate = [morningFireDate randomizeForDayPeriod:morningFireDate.dayPeriod];
        UILocalNotification *noonNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:noonFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:noonNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:noonFireDate];
        
        NSDate *eveningFireDate = [noonFireDate randomizeForDayPeriod:noonFireDate.dayPeriod];
        UILocalNotification *eveningNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:eveningFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:eveningNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:eveningFireDate];
    } else if (dayPeriod == DayPeriodNoon) {
        NSDate *noonFireDate = [date randomizeForDayPeriod:dayPeriod];
        UILocalNotification *noonNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:noonFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:noonNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:noonFireDate];
        
        NSDate *eveningFireDate = [noonFireDate randomizeForDayPeriod:noonFireDate.dayPeriod];
        UILocalNotification *eveningNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:eveningFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:eveningNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:eveningFireDate];
    } else if (dayPeriod == DayPeriodEvening) {
        NSDate *eveningFireDate = [date randomizeForDayPeriod:dayPeriod];
        UILocalNotification *eveningNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:eveningFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:eveningNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:eveningFireDate];
    }
    
    NSDate *nextDayDate = date;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
//    NSCalendarUnit units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    
    for (NSInteger i = 0; i < days; ++i) {
        
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.day = 1;
        
        nextDayDate = [calendar dateByAddingComponents:components toDate:nextDayDate options:NSCalendarMatchNextTimePreservingSmallerUnits];
        nextDayDate = [calendar dateBySettingHour:(calendar.timeZone.secondsFromGMT / 3600) minute:0 second:1 ofDate:nextDayDate options:NSCalendarMatchNextTimePreservingSmallerUnits];
        
        NSDate *morningFireDate = [nextDayDate randomizeForDayPeriod:nextDayDate.dayPeriod];
        UILocalNotification *morningNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:morningFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:morningNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:morningFireDate];
        
        NSDate *noonFireDate = [morningFireDate randomizeForDayPeriod:morningFireDate.dayPeriod];
        UILocalNotification *noonNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:noonFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:noonNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:noonFireDate];
        
        NSDate *eveningFireDate = [noonFireDate randomizeForDayPeriod:noonFireDate.dayPeriod];
        UILocalNotification *eveningNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:eveningFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:eveningNotification];
        [self addReminders:remindersCount forNotificationCategory:(NSString *)kSnoozableNotificationCategoryIdentifier afterMainFireDate:eveningFireDate];
    }
}

+ (void)scheduleReminderNotificationAfterMinutes:(NSTimeInterval)minutes {
    NSDate *fireDate = [NSDate dateWithTimeIntervalSinceNow:minutes * 60];
    UILocalNotification *notification = [self notificationForCategory:(NSString *)kReminderNotificationCategoryIdentifier atDate:fireDate];
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    [self saveReminderNotificationForDate:fireDate];
    [self rescheduleDailyNotificationsAfterReminderDate:fireDate];
    [self addReminders:kNotificationRemindersCount forNotificationCategory:(NSString *)kReminderNotificationCategoryIdentifier afterMainFireDate:fireDate];
    
}

+ (void)saveReminderNotificationForDate:(NSDate *)date {
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:(NSString *)kReminderNotificationFireDate];
}

+ (NSDate *)restoreReminderNotification {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *fireDate = [defaults objectForKey:(NSString *)kReminderNotificationFireDate];
    if ([fireDate isKindOfClass:[NSDate class]] == false) {
        return nil;
    }
    if ([fireDate compare:[NSDate date]] == NSOrderedDescending) {
        UILocalNotification *notification = [self notificationForCategory:(NSString *)kReminderNotificationCategoryIdentifier atDate:fireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        [self addReminders:kNotificationRemindersCount forNotificationCategory:(NSString *)kReminderNotificationCategoryIdentifier afterMainFireDate:fireDate];
        return fireDate;
    } else {
        [defaults removeObjectForKey:(NSString *)kReminderNotificationFireDate];
        return nil;
    }
}

#pragma mark - Private

+ (UILocalNotification *)notificationForCategory:(NSString *)category atDate:(NSDate *)date {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.timeZone = [NSTimeZone localTimeZone];
    notification.fireDate = date;
    notification.alertAction = @"פתח כאן";
    notification.alertBody = @"האפליקציה דורשת את תשומת ליבך, אנא הפעל אותה";
    notification.soundName = @"notification_tone__2.mp3";
    notification.category = category;
    return notification;
}

+ (void)rescheduleDailyNotificationsAfterReminderDate:(NSDate *)reminderDate {
    [self cancelNotificationsForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier];
    [self scheduleSnoozableNotificationForDays:kFurtherNotificationsDaysCount startingFromDate:reminderDate withRemindersCount:kNotificationRemindersCount];
}

+ (void)addReminders:(NSInteger)remindersCount forNotificationCategory:(NSString *)category afterMainFireDate:(NSDate *)fireDate {
    // schedule `remindersCount` reminders every ten minutes after this notification
    NSTimeInterval tenMinutes = 60 * 10;
    for (NSInteger i = 1; i < remindersCount; i++) {
        NSDate *reminderFireDate = [fireDate dateByAddingTimeInterval:(tenMinutes * i)];
        UILocalNotification *reminderNotification = [self notificationForCategory:(NSString *)kSnoozableNotificationCategoryIdentifier atDate:reminderFireDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:reminderNotification];
    }
}

@end
