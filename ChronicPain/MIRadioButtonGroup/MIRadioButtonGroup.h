//
//  MIRadioButtonGroup.h
//  ChronicPain
//
//  Created by noah alevi on 12/6/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MIRadioButtonGroup : UIView {
    NSMutableArray *radioButtons;
    int choise;    
}

@property (nonatomic,retain) NSMutableArray *radioButtons;
- (id)initWithFrame:(CGRect)frame andOptions:(NSArray *)options andColumns:(int)columns;
-(IBAction) radioButtonClicked:(UIButton *) sender;
-(void) removeButtonAtIndex:(int)index;
-(void) setSelected:(int) index;
-(void)clearAll;
//get the users choise
-(int)getChoise;
@end
