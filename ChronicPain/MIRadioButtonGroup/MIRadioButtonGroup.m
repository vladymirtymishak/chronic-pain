//
//  MIRadioButtonGroup.m
//  ChronicPain
//
//  Created by noah alevi on 12/6/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//
#import "MIRadioButtonGroup.h"

@implementation MIRadioButtonGroup
@synthesize radioButtons;



- (id)initWithFrame:(CGRect)frame andOptions:(NSArray *)options andColumns:(int)columns{
    choise=-1;
    
    NSMutableArray *arrTemp =[[NSMutableArray alloc]init];
    self.radioButtons =arrTemp;
    if (self = [super initWithFrame:frame]) {
        // Initialization code
        int framex =0;
        framex= frame.size.width/columns;
        int framey = 0;
        framey =frame.size.height/([options count]/(columns));
        int rem =[options count]%columns;
        if(rem !=0){
            framey =frame.size.height/(([options count]/columns)+1);
        }
        int k = 0;
        for(int i=0;i<([options count]/columns);i++){
            for(int j=0;j<columns;j++){
                
                int x = framex*0.25;
                int y = framey*0.25;
                UIButton *btTemp = [[UIButton alloc]initWithFrame:CGRectMake(framex*j+x, framey*i+y, framex/2+x, framey/2+y)];
           //     [[btTemp layer] setBorderWidth:2.0f];
            //    [[btTemp layer] setBorderColor:[UIColor orangeColor].CGColor];
               // [string sizeWithAttributes:
                // @{NSFontAttributeName: [UIFont systemFontOfSize:14]}];
                CGSize stringsize = [[options objectAtIndex:k] sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]}];//sizeWithFont:[UIFont systemFontOfSize:14]];
                [btTemp setImageEdgeInsets:UIEdgeInsetsMake(0,0, 0,-stringsize.width*1.2)];
                [btTemp setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, btTemp.imageView.image.size.width + 40)];
                [btTemp addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                btTemp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                [btTemp setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
                [btTemp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                btTemp.titleLabel.font =[UIFont systemFontOfSize:17];
                [btTemp setTitle:[options objectAtIndex:k] forState:UIControlStateNormal];
                


                [self.radioButtons addObject:btTemp];
                [self addSubview:btTemp];
                k++;
                
            }
        }
        
        for(int j=0;j<rem;j++){
            
            int x = framex*0.25;
            int y = framey*0.25;
            UIButton *btTemp = [[UIButton alloc]initWithFrame:CGRectMake(framex*j+x, framey*([options count]/columns), framex/2+x, framey/2+y)];
            [btTemp addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            btTemp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btTemp setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
            [btTemp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btTemp.titleLabel.font =[UIFont systemFontOfSize:14.f];
            [btTemp setTitle:[options objectAtIndex:k] forState:UIControlStateNormal];
            [self.radioButtons addObject:btTemp];
            [self addSubview:btTemp];
           
            k++;
            
            
        }
        
    }
    return self;
}

- (void)dealloc {
   
}

-(IBAction) radioButtonClicked:(UIButton *) sender{
    for(int i=0;i<[self.radioButtons count];i++){
        if (self.radioButtons[i]==sender) {
            choise=i;
        }
        [[self.radioButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        
    }
    [sender setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateNormal];
    

}

-(void) removeButtonAtIndex:(int)index{
    [[self.radioButtons objectAtIndex:index] removeFromSuperview];
    
}

-(void) setSelected:(int) index{
    for(int i=0;i<[self.radioButtons count];i++){
        [[self.radioButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        
    }
    [[self.radioButtons objectAtIndex:index] setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateNormal];
    
    
}

-(void)clearAll{
    for(int i=0;i<[self.radioButtons count];i++){
        [[self.radioButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        
    }
    
}
-(int)getChoise{
    return choise;
}

@end
