//
//  DetailsHelper.m
//  ChronicPain
//
//  Created by noah alevi on 10/24/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//


#import "DetailsHelper.h"

@implementation DetailsHelper
//const strings
/**
 this URl called just one time 
 */
NSString * const urlActivatePatient = @"https://medinfo2.ise.bgu.ac.il/ChronicPainTest/WCF/service1.svc/jsonp/ActivatePatient";
NSString * const urlCanRunApp =@"https://medinfo2.ise.bgu.ac.il/ChronicPainTest/WCF/service1.svc/jsonp/CanStartApplication";

//detailds methods
+(NSString*)userIdFromDevice{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [defaults objectForKey:@"userId"];
    return userId;
}

+(NSString*) userPhoneNumber{
    static NSString* str= @"0508651068";
    return str;
}
//TESTING URLS

+(NSString*) SERVICE_URL
{
    //TEST : https://medinfo2.ise.bgu.ac.il/ChronicPainTest/WCF/
    // "PROD: https://medinfo2.ise.bgu.ac.il/ChronicPain/WCF/"
    //@"http://chronicpaincs.cloudapp.net/"
    //@"http://chronicpainws.cloudapp.net/"
    static NSString* str= @"https://medinfo2.ise.bgu.ac.il/ChronicPainTest/WCF/";
    return str;
}

//SERVICES PATH


+(NSString*) ActivatePatient
{
    static NSString* str=@"service1.svc/jsonp/ActivatePatient";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}

+(NSString*) UrlExistingPhone
{
    static NSString* str=@"service1.svc/jsonp/ExistingPhone";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}
+(NSString*) urlgetPatientId
{
    static NSString* str=@"service1.svc/jsonp/GetPatientID";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}
+(NSString*) UrlGetPatientName
{
    static NSString* str=@"service1.svc/jsonp/GetPatientNameByPhone";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}

+(NSString*) UrlCanWakeUpRandomly
{
    static NSString* str=@"service1.svc/jsonp/CanWakeUpRandomly";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}
+(NSString*) UrlNewPatientAlert
{
    static NSString* str=@"service1.svc/jsonp/NewPatientAlert";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}+(NSString*) UrlIsHalfAnHourPassed
{
    static NSString* str=@"service1.svc/jsonp/CanRunRandomReport";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}
+(NSString*) urlGetPatientMainTerapistPhone
{
    static NSString* str=@"service1.svc/jsonp/GetPatientMainTerapistPhone";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}
+(NSString*) urlGetPatientNameByPhone
{
    static NSString* str=@"service1.svc/jsonp/GetPatientNameByPhone";
    str = [[self SERVICE_URL] stringByAppendingString:str];
    return str;
}

//JSON PARAMS
+(NSString*) ACTIVATE_SERVICE_PARAMETER_NAME
{
    static NSString* str= @"patientInitCode";
    return str;
}




//PICARD CONNECTIONS METHODS
+(NSString*) runService:(int)paramNum usingUrl :(NSString*)url withObjects : (NSArray*)objs andKeys : (NSArray*)keys

{
    //init json to send
    NSDictionary * dictionary = [[NSDictionary alloc] initWithObjects:objs forKeys:keys];
    NSData * jsonDict = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:jsonDict encoding:(NSUTF8StringEncoding)];
    //SET HTTP request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"text/javascript" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    //response
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString * responseStr = [[NSString alloc]initWithData:response encoding:NSUTF8StringEncoding];
    return responseStr;
    
    }
+(void) runService2:(CompletionBlock)completionBlock usingUrl :(NSString*)url withObjects : (NSArray*)objs andKeys : (NSArray*)keys
{
    //init json to send
    NSDictionary * dictionary = [[NSDictionary alloc] initWithObjects:objs forKeys:keys];
    NSData * jsonDict = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:jsonDict encoding:(NSUTF8StringEncoding)];
    //SET HTTP request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"text/javascript" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
         if (error) {
             NSLog(@"[DetailsHelper] runService2 error: %@", error);
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSString * responseStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                 completionBlock(YES,responseStr, error);
             });
         }
     }];
}

+(NSString*)activatePatient:(NSString*)userId{
    
    NSArray* objs = @[userId];
    NSArray* keys = @[[self ACTIVATE_SERVICE_PARAMETER_NAME]];
     return [DetailsHelper runService:1 usingUrl:urlActivatePatient withObjects:objs andKeys:keys];
    
}

+ (void)activatePatient:(NSString*)userId withCompletionBlock:(CompletionBlock)completionBlock
{
    NSArray* objs = @[userId];
    NSArray* keys = @[[self ACTIVATE_SERVICE_PARAMETER_NAME]];
    [self runService2:completionBlock usingUrl:urlActivatePatient withObjects:objs andKeys:keys];
}


+(void)canRunApp:(NSString*)userId withCompletionBlock:(CompletionBlock)completionBlock
{
    NSArray* objs = @[userId];
    NSArray* keys = @[@"id"];
    [self runService2:completionBlock usingUrl:urlCanRunApp withObjects:objs andKeys:keys];
}


+(void)registerUser:(NSString *)userId{
    NSUInteger len = userId.length-2;
    NSString *subStr = [userId substringWithRange:NSMakeRange(1, len)];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:subStr forKey:@"userId"];
    [defaults synchronize];
}

@end
