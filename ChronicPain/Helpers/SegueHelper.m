//
//  SegueHelper.m
//  ChronicPain
//
//  Created by noah alevi on 3/23/15.
//  Copyright (c) 2015 BGU. All rights reserved.
//

#import "SegueHelper.h"

@implementation SegueHelper

//get the next screen String segue
+ (NSString *)nextScreen:(NSDictionary *)responseJson optionsScreen:(NSString *)option degOfPainScreen:(NSString *)degOfPain reccomendationScreen:(NSString *)reccomendation
{
    int fatherId = [self getFatherResponseId:responseJson];
    int sonId = [self getSonResponseId:responseJson];
    
    NSArray* optionArr = @[@17931, @18369, @17824];
    NSArray* degOfPainArr = @[@20271, @17783, @17892];
    NSArray* recommendation = @[@17828, @17801, @20277, @20278, @20148, @20149, @20146, @17798, @17828, @17828, @17829, @17800, @20274, @17800, @20148, @20149, @20412, @20269, @20274, @20277, @20278, @17798, @17828, @17829];
    
    if([degOfPainArr containsObject:@(sonId)]) {
        return degOfPain;
    }
    
    if ([optionArr containsObject:@(fatherId)]) {
        return option;
    }
    
    if ([recommendation containsObject:@(fatherId)] || [recommendation containsObject:@(sonId)]) {
        return reccomendation;
    }
    
    return @"Didnt found!!";
    
}

//get father Response ID
+ (int)getFatherResponseId:(NSDictionary *)json
{
    return ([[json objectForKey:@"CurrentResponseID"] intValue]);
}

//get son response id
+ (int)getSonResponseId:(NSDictionary *)json
{
    NSArray* messages = [json objectForKey:@"Messages"];
    NSDictionary* mes = [messages objectAtIndex:0];
    return ([[mes objectForKey:@"GuidelineID"] intValue]);
}

+ (void)setTimer:(int)time {
    NSDate *timer = [[NSDate date] dateByAddingTimeInterval:time * 60];
    [[NSUserDefaults standardUserDefaults] setObject:timer forKey:@"timer"];
}

+ (void)showTimerAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"האפליקציה ננעלה"
                                                    message:@"אנא המתן עד כחלוף שעה מנטילת התרופה"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert show];
 
}

+ (NSString *)getMessageCotent:(NSDictionary *)json {
    NSArray *message = [json objectForKey:@"Messages"];
    NSDictionary *dict = message[0];
    return [dict valueForKey:@"MessageName"];
}

+ (UIColor *)colorWithHexString:(NSString *)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
       return [UIColor grayColor];
    }
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) {
        cString = [cString substringFromIndex:2];
    }
    
    if ([cString length] != 6) {
        return  [UIColor grayColor];
    }
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
