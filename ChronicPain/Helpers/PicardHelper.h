//
//  PicardHelper.h
//  ChronicPain
//
//  Created by noah alevi on 11/13/14.
//  Copyright (c)2014 BGU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailsHelper.h"

typedef void (^myCompletionBlock)(BOOL success, NSDictionary *responseJson, NSError *error);

@interface PicardHelper : NSObject

+ (NSString *)currentDate;

+ (NSString *)PICARD_URL;
+ (NSString *)DAILY_REPORT_PROTOCOL;

//picard connection helpers
+ (void)StartNewSessionForJson:(myCompletionBlock)completionBlock;
+ (void)GetRecommendations:(NSMutableDictionary*)jsonResponse withCompletionBlock:(myCompletionBlock)completionBlock;
+ (void)runService2:(myCompletionBlock)completionBlock usingUrl:(NSString*)url withObjects:(NSArray*)objs andKeys:(NSArray*)key;

//picard error message
+ (void)showErrorAlert;

@end
