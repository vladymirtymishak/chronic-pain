//
//  DetailsHelper.h
//  ChronicPain
//
//  Created by noah alevi on 10/24/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailsHelper : NSObject
typedef void (^CompletionBlock)(BOOL success, NSString *response, NSError *error);
//details methods
+(NSString*)userIdFromDevice;
+(NSString*) userPhoneNumber;

+(NSString*) SERVICE_URL;
//service urls
+(NSString*) ActivatePatient;
+(NSString*) UrlExistingPhone;
+(NSString*) urlgetPatientId;
+(NSString*) UrlGetPatientName;
+(NSString*) UrlCanWakeUpRandomly;
+(NSString*) UrlNewPatientAlert;
+(NSString*) UrlIsHalfAnHourPassed;
+(NSString*) urlGetPatientMainTerapistPhone;


//JSON PARAMS
+(NSString*) ACTIVATE_SERVICE_PARAMETER_NAME;
//run JSON service
+(NSString*) runService:(int)paramNum usingUrl :(NSString*)url withObjects : (NSArray*)objs andKeys : (NSArray*)keys;

+(void) runService2:(CompletionBlock)completionBlock usingUrl :(NSString*)url withObjects : (NSArray*)objs andKeys : (NSArray*)keys;
//register user on mobile device
+(void)registerUser:(NSString*)userId;

//service methods*****
//activate user and get user's key
+(NSString*)activatePatient:(NSString*)userId;


+ (void)activatePatient:(NSString*)userId withCompletionBlock:(CompletionBlock)completionBlock;
//is the user can start the app

+(void)canRunApp:(NSString*)userId withCompletionBlock:(CompletionBlock)completionBlock;

@end
