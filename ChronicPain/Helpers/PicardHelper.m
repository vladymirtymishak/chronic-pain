//
//  PicardHelper.m
//  ChronicPain
//
//  Created by noah alevi on 11/13/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import "PicardHelper.h"

@implementation PicardHelper

NSString *const urlGetRecommendation = @"https://medinfo2.ise.bgu.ac.il/Picard/PicardWCFServicesTest/PicardWCFServer.ClientEMRService.svc/jsonp/GetRecommendationsForJson";
NSString *const urlStartNewSession = @"https://medinfo2.ise.bgu.ac.il/Picard/PicardWCFServicesTest/PicardWCFServer.ClientEMRService.svc/jsonp/StartNewSessionForJson";

//get current date to Json
+ (NSString *)currentDate
{
    NSDate *date = [NSDate date];
    
    // adding 3 hours for Israel summer time
    CGFloat duration = ([date timeIntervalSince1970] + 60 * 60 * 3) * 1000;

    NSString *check = [NSString stringWithFormat:@"%.0lf",duration];
    NSString *ans = [@"/Date(" stringByAppendingString:check];
    ans = [ans stringByAppendingString:@")/"];

    return ans;
}

+ (NSString *)PICARD_URL
{
    // USER PASS xSxrINXyDKHFRePO sRKIMntAKXEcOtrT
    // testing
    //https://medinfo2.ise.bgu.ac.il/Picard/PicardWCFServicesTest/PicardWCFServer.ClientEMRService.svc/jsonp/
    
    //PRODUCTION:
    //https://medinfo2.ise.bgu.ac.il/Picard/PicardWCFServices/PicardWCFServer.ClientEMRService.svc/jsonp/XXXXXX
    
    static NSString *str= @"https://medinfo2.ise.bgu.ac.il/Picard/PicardWCFServicesTest/PicardWCFServer.ClientEMRService.svc/jsonp/";
    return str;
}

+ (NSString *)DAILY_REPORT_PROTOCOL
{
    static NSString *str= @"16851";
    return str;
}

+ (void)runService2:(myCompletionBlock)completionBlock usingUrl :(NSString *)url withObjects : (NSArray*)objs andKeys : (NSArray*)keys
{
    //init json to send
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjects:objs forKeys:keys];

    
    //try NSJsonserialzation
    NSData *jsonDict = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonDict encoding:(NSUTF8StringEncoding)];
   
    //manipulate str
    jsonString=[PicardHelper orderMessages:jsonString];
    
    //SET HTTP request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL
                                                 URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"text/javascript" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];

    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if (error) {
            NSLog(@"[PicardHelper] runService2 error: %@", error);
        } else {
            NSString *responseStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSError *error;
            NSDictionary *responseJson;
            @try{
                responseJson = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            }
            @catch(NSException*){
                completionBlock(NO,nil, error);
            }
            if (!error) {
                completionBlock(YES,responseJson, nil);//call to completion block with success
            }else{
                completionBlock(NO,nil, error);
            }
           // return responseJson;
        }
    }];
}

//error message from server
+ (void)showErrorAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה"
                                                    message:@"בעיה בבקשה לשרת - אנא נסה מאוחר יותר"
                                                   delegate:nil
                                          cancelButtonTitle:@"אישור"
                                          otherButtonTitles:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert show];
    });
}

//start new picard session
+ (void)StartNewSessionForJson:(myCompletionBlock)completionBlock
{
   // NSString *dailyReportProtocol = [self DAILY_REPORT_PROTOCOL];
    NSString *userId = [DetailsHelper userIdFromDevice];
    NSString *date = [self currentDate];
   
    NSArray *objs = @[userId, @"xxx", @"Chronic Pain", @"17777", @"ICU", @"NoClinicalSettingsType", date];
    /*
     all the parameters don't change exept the UserID and the date
     */
    NSArray *keys = @[@"patientID", @"patientName", @"glTitle", @"glDocID", @"clinicName", @"ClinicalSettingsType", @"startTime"];
    return [self runService2:completionBlock usingUrl:urlStartNewSession withObjects:objs andKeys:keys];
}

//getReccomendationsForJson
+ (void)GetRecommendations:(NSMutableDictionary *)jsonResponse withCompletionBlock:(myCompletionBlock)completionBlock
{
    NSArray *keys = @[@"sessionDetails"];
    NSArray *objs = @[jsonResponse];
    [self runService2: completionBlock usingUrl:urlGetRecommendation withObjects:objs andKeys:keys];
}

+ (NSString *)orderMessages:(NSString *)jsonString
{
    NSString *typeStr =  @"\"__type\"";
    NSUInteger length = [jsonString length];
    NSRange range = NSMakeRange(0, length);
    NSRange startRange = NSMakeRange(0, length);
    NSRange endRange = NSMakeRange(0, length);
    NSString *prefix, *message;

    //check if there is "__type" substring in the json string
    range = [jsonString rangeOfString:typeStr options:0 range:range];
    if (range.location == NSNotFound) {
        return jsonString;
    }
    
    //init prefix
    range = NSMakeRange(0,range.location);
    range = [jsonString rangeOfString:@"{" options:NSBackwardsSearch range:range];
    prefix = [jsonString substringToIndex:range.location-1];
   
    range = [jsonString rangeOfString:typeStr];
    while(range.location!= NSNotFound)
    {
        range = [jsonString rangeOfString:typeStr options:0 range:range];
        startRange =  NSMakeRange(0, range.location);
        if (range.location != NSNotFound)
        {
            //build message
           startRange = [jsonString rangeOfString:@"{" options:NSBackwardsSearch range:startRange];
            startRange.length = length-startRange.location;
            endRange = [jsonString rangeOfString:@"}" options:0 range:startRange];
            endRange = NSMakeRange(startRange.location, endRange.location-startRange.location+2);
            message = [jsonString substringWithRange:endRange];
            //add to prefix
            message = [self manipulateMessage:message];
            prefix = [prefix stringByAppendingString:message];

           //increse range;
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
        } else {
            endRange = NSMakeRange(endRange.length+endRange.location,length - (endRange.length+endRange.location));
            message = [jsonString substringWithRange:endRange];
            prefix = [prefix stringByAppendingString:message];
        }
    }//end while
            //  delete spaces and \n from str!
    prefix = [prefix stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    prefix = [prefix stringByReplacingOccurrencesOfString:@" " withString:@""];
    return prefix;
}

+ (NSString *)manipulateMessage:(NSString *)message
{
    NSRange startRange,endRange;
    NSString *typeStr =  @"\"__type\"";
    NSMutableString *mutableMessage=[message mutableCopy];
    //manipulate message
    //find range of "type"
    startRange = [message rangeOfString:typeStr];
    endRange = NSMakeRange( startRange.location-1, message.length-  startRange.location-1);
    endRange = [message rangeOfString:@"," options:0 range:endRange];
    startRange = NSMakeRange(startRange.location-9, endRange.location-startRange.location+10);
    message = [message substringWithRange:startRange];
 
    //replace
    [mutableMessage deleteCharactersInRange:startRange];
    [mutableMessage insertString:message atIndex:1];
    return mutableMessage;
}

@end
