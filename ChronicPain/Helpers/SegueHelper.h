//
//  SegueHelper.h
//  ChronicPain
//
//  Created by noah alevi on 3/23/15.
//  Copyright (c) 2015 BGU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SegueHelper : NSObject

+(NSString*)nextScreen:(NSDictionary*)responseJson optionsScreen:(NSString*)option degOfPainScreen : (NSString*) degOfPain  reccomendationScreen : (NSString*) reccomendation;

+(int)getFatherResponseId:(NSDictionary*)json;
+(int)getSonResponseId:(NSDictionary*)json;
+(void)showTimerAlert;
+(void)setTimer:(int) time;

//get json message inner text
+(NSString*) getMessageCotent:(NSDictionary*)json;

//sets label background color
+(UIColor*)colorWithHexString:(NSString*)hex;
@end
