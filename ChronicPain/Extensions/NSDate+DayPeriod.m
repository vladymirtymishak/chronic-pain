//
//  NSDate+DayPeriod.m
//  ChronicPain
//
//  Created by Vladimir Tymishak on 25/01/2018.
//  Copyright © 2018 BGU. All rights reserved.
//

#import "NSDate+DayPeriod.h"

@implementation NSDate (DayPeriod)

- (DayPeriod)dayPeriod {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger hour = [calendar component:NSCalendarUnitHour fromDate:self];
    
    if (hour >= DayPeriodEvening || hour < DayPeriodMorning) {
        return DayPeriodMorning;
    }
    
    if (hour >= DayPeriodMorning && hour < DayPeriodNoon) {
        return DayPeriodNoon;
    }
    
    if (hour >= DayPeriodNoon && hour < DayPeriodEvening) {
        return DayPeriodEvening;
    }
    
    return DayPeriodUnknown;
}

- (NSDate *)randomizeForDayPeriod:(DayPeriod)dayPeriod {
    NSInteger randomHour = dayPeriod + (arc4random() % 2);
    NSInteger randomMinute = (arc4random() % 60);
    NSInteger randomSecond = (arc4random() % 60);
    
    NSDate *date = [[NSCalendar currentCalendar] dateBySettingHour:randomHour
                                                            minute:randomMinute
                                                            second:randomSecond
                                                            ofDate:self
                                                           options:kNilOptions];
    return date;
}

@end
