//
//  NSDate+DayPeriod.h
//  ChronicPain
//
//  Created by Vladimir Tymishak on 25/01/2018.
//  Copyright © 2018 BGU. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DayPeriod) {
    DayPeriodUnknown = NSNotFound,
    DayPeriodMorning = 7,
    DayPeriodNoon = 13,
    DayPeriodEvening = 19,
};

@interface NSDate (DayPeriod)

@property (nonatomic, readonly) DayPeriod dayPeriod;

- (NSDate *)randomizeForDayPeriod:(DayPeriod)dayPeriod;

@end
