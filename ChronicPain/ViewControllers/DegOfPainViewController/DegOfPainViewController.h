//
//  DegOfPainViewController.h
//  ChronicPain
//
//  Created by noah alevi on 9/7/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailsHelper.h"
#import "PicardHelper.h"
#import "SegueHelper.h"

@interface DegOfPainViewController : UIViewController

@property (nonatomic, strong) NSDictionary *responseJson;
@property (nonatomic, strong) NSString *choiseStr;

@end
