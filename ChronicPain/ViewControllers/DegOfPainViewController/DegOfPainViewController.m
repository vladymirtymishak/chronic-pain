//
//  DegOfPainViewController.m
//  ChronicPain
//
//  Created by noah alevi on 9/7/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import "DegOfPainViewController.h"
#import "OptionViewController.h"
#import "RecommendationViewController.h"

@interface DegOfPainViewController ()

@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *painDegree;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@end

@implementation DegOfPainViewController

@synthesize choiseStr;
//user choise init to -1
NSInteger choise;

//degree of pain buttons
- (IBAction)buttonClicked:(id)sender {
    //change users choise when button clicked
    choise = ((UIControl *)sender).tag;
    NSString *buttonNumber = [NSString stringWithFormat:@"%li", ((UIControl*)sender).tag];
    
    choiseStr = buttonNumber;
    _painDegree.text = buttonNumber;
}

//next button action
- (IBAction)nextView:(id)sender {
    if (choise == -1) {
        [self showAlert];
    } else {
        self.view.alpha = 0.3;
        [self.loadingSpinner startAnimating];
        [self sendDataToServer];
    }
}

- (void)sendDataToServer {
    NSMutableDictionary*responseMutable = [self.responseJson mutableCopy];
    
    NSMutableArray *messages = [responseMutable objectForKey:@"Messages"];
    NSDictionary* message = messages[0];
    NSMutableDictionary* m = [message mutableCopy];
    //put str of choise in result
    [m setObject:choiseStr forKey:@"ResultContent"];

    [messages replaceObjectAtIndex:0 withObject:m];
    [PicardHelper GetRecommendations:responseMutable withCompletionBlock:^(BOOL success, NSDictionary *responseJson, NSError *error) {
        if (success) {
            //Move to next view
            self.responseJson = responseJson;
            NSString *segueStr = [SegueHelper nextScreen:self.responseJson optionsScreen:@"PainToOption" degOfPainScreen:@"Self" reccomendationScreen:@"PainToRecommendation"];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.alpha = 1;
                [self.loadingSpinner stopAnimating];
                [self performSegueWithIdentifier: segueStr sender: self];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.alpha = 1;
                [self.loadingSpinner stopAnimating];
            });
            [PicardHelper showErrorAlert];
        }
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.button.layer.cornerRadius = 3;
    self.button.layer.borderWidth = 1;
    self.button.layer.borderColor = [UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1].CGColor;
    choiseStr = @"-1";
    choise = -1;
}


- (void)showAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה"
                                                    message:@"אנא בחר רמת כאב לפני מעבר למסך הבא"
                                                   delegate:nil
                                          cancelButtonTitle:@"אישור"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"PainToOption"]){
        OptionViewController *vc = [segue destinationViewController];
        vc.responseJson = self.responseJson;
    }
    if ([[segue identifier] isEqualToString:@"PainToRecommendation"]){
        RecommendationViewController *vc = [segue destinationViewController];
        vc.responseJson = self.responseJson;
    }
}

@end
