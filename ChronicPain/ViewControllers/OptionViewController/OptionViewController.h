//
//  OptionViewController.h
//  ChronicPain
//
//  Created by noah alevi on 11/23/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIRadioButtonGroup.h"
#import "DetailsHelper.h"
#import "PicardHelper.h"
#import "SegueHelper.h"


@interface OptionViewController : UIViewController

@property (nonatomic, strong) __block NSDictionary *responseJson;
@property (nonatomic, strong) MIRadioButtonGroup* radiobuttons;

@end
