//
//  OptionViewController.m
//  ChronicPain
//
//  Created by noah alevi on 11/23/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import "OptionViewController.h"
#import "DegOfPainViewController.h"
#import "RecommendationViewController.h"
#import "Notifications.h"

@interface OptionViewController ()

@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
@property (strong, nonatomic) NSMutableArray *arrayOfTimesFromServer;

@end

@implementation OptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayOfTimesFromServer = [[NSMutableArray alloc] init];
    
    self.textLabel.text = [self.responseJson objectForKey:@"CurrentResponseTitle"];
    
    [self makeAnswers];
    [self makeCuntinueButton];
}

- (void)makeCuntinueButton {
    UIButton *oldButton = [self.view viewWithTag:321];
    if (oldButton) {
        [oldButton removeFromSuperview];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [button addTarget:self action:@selector(ButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"המשך" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:20];
    button.tintColor = [UIColor whiteColor];
    button.backgroundColor = [UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1];
    button.layer.cornerRadius = 3;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1].CGColor;
    [button setContentEdgeInsets:UIEdgeInsetsMake(10, 30, 10, 30)];
    button.tag = 321;
    
    [self.view addSubview:button];
    
    NSMutableArray *constraints = [[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[button]-50-|" options:0 metrics:nil views:@{@"button": button}] mutableCopy];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[options]-30-[button]" options:0 metrics:nil views:@{@"button": button, @"options": self.radiobuttons}]];
    [self.view addConstraints:constraints];
}

- (void)makeAnswers {
    NSArray *messages = [self.responseJson objectForKey:@"Messages"];
    NSMutableArray *opt = [[NSMutableArray alloc] init];
    NSDictionary *message;
    for (int i = 0; i < messages.count; i++) {
        message = messages[i];
        [opt addObject:[message objectForKey:@"ConceptName"]];
    }
    MIRadioButtonGroup *group = [[MIRadioButtonGroup alloc]initWithFrame:CGRectMake(20, 120, 280, 270) andOptions:opt andColumns:1];
    group.translatesAutoresizingMaskIntoConstraints = NO;
    self.radiobuttons = group;
    self.arrayOfTimesFromServer = opt;
    [self.view addSubview:group];
    
    NSMutableArray *constraints = [[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[group]-|" options:0 metrics:nil views:@{@"group": group}] mutableCopy];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-120-[group(270)]-|" options:0 metrics:nil views:@{@"group": group}]];
    [self.view addConstraints:constraints];
}

- (IBAction)ButtonClicked:(UIButton *)sender {
    if ([self.radiobuttons getChoise] == -1) {
        [self showAlert];
    } else {
        self.view.alpha = 0.3;
        int time = 0;
        
        [self sendDataToServer];
        
        switch ([self.radiobuttons getChoise]) {
            case 0:
                if ([self.arrayOfTimesFromServer[0] isEqualToString:@"20 דקות"]) {
                    time = 40;
                } else if ([self.arrayOfTimesFromServer[0] isEqualToString:@"0-30 דקות"]) {
                    time = 60;
                }
                break;
            case 1:
                if ([self.arrayOfTimesFromServer[1] isEqualToString:@"40 דקות"]) {
                    time = 20;
                } else if ([self.arrayOfTimesFromServer[1] isEqualToString:@"30-60 דקות"]) {
                    time = 30;
                }
                break;
            default:
                break;
        }
        if (time > 0) {
            [SegueHelper setTimer:time];
            [Notifications scheduleReminderNotificationAfterMinutes:time];
        }
    }
}

- (void)reloadView {
    [self.radiobuttons removeFromSuperview];
    [self viewDidLoad];
    [self.loadingSpinner stopAnimating];
}

- (void)sendDataToServer {
    NSMutableDictionary*responseMutable = [self.responseJson mutableCopy];
    NSMutableArray *messages = [self.responseJson objectForKey:@"Messages"];
    for (int i=0; i<messages.count; i++) {
        NSDictionary* message = messages[i];
        NSMutableDictionary* m = [message mutableCopy];
        //put bool in answers
        if (i==[self.radiobuttons getChoise])
            [m setObject:@"TRUE" forKey:@"ResultContent"];
        else
            [m setObject:@"FALSE" forKey:@"ResultContent"];
        [messages replaceObjectAtIndex:i withObject:m];
    }
    
    [responseMutable setObject:messages forKey:@"Messages"];
    
    [self.loadingSpinner startAnimating];
    
    [PicardHelper GetRecommendations:responseMutable withCompletionBlock:^(BOOL success, NSDictionary *responseJson, NSError *error) {
        if (success) {
            NSLog(@"[OptionViewController] received response for get recomendations:\n%@", responseJson);
            self.responseJson = responseJson;
            NSString* segueStr = [SegueHelper nextScreen:self.responseJson optionsScreen:@"Self" degOfPainScreen:@"OptionsToDegOfPain" reccomendationScreen:@"OptionToRecommendation"];
            dispatch_async(dispatch_get_main_queue(),^{
                self.view.alpha = 1;
                [self.loadingSpinner stopAnimating];
                if(![segueStr isEqual:@"Self"]) {
                    [self performSegueWithIdentifier: segueStr sender: self];
                } else {
                    [self reloadView];
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(),^{
                self.view.alpha = 1;
                [self.loadingSpinner stopAnimating];
            });
            [PicardHelper showErrorAlert];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"OptionsToDegOfPain"]){
        DegOfPainViewController *vc = [segue destinationViewController];
        vc.responseJson = self.responseJson;
    }
    if ([[segue identifier] isEqualToString:@"OptionToRecommendation"]){
        RecommendationViewController *vc = [segue destinationViewController];
        vc.responseJson = self.responseJson;
    }
}

- (void)showAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"אנא בצע בחירה לפני מעבר למסך הבא"
                                                   delegate:nil
                                          cancelButtonTitle:@"אישור"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
