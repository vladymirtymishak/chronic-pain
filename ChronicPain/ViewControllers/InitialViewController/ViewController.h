//
//  ViewController.h
//  ChronicPain
//
//  Created by noah alevi on 9/7/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailsHelper.h"
#import "PicardHelper.h"

@interface ViewController : UIViewController

@property (nonatomic, strong)__block NSDictionary *responseJson;

@end
