//
//  ViewController.m
//  ChronicPain
//
//  Created by noah alevi on 9/7/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import "ViewController.h"
#import "DetailsHelper.h"
#import "PicardHelper.h"
#import "SegueHelper.h"
#import "DegOfPainViewController.h"
#import "OptionViewController.h"
#import "RecommendationViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *textLabel1;
@property (weak, nonatomic) IBOutlet UITextField *patientIdText;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UILabel *textLabel2;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@end

@implementation ViewController

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

//connect to server and register user
- (IBAction)connectToServer:(id)sender {
    [self.view endEditing:YES];
    [self.loadingSpinner setHidden:NO];
    [self.loadingSpinner startAnimating];
    self.view.alpha = 0.3;
    [DetailsHelper activatePatient:self.patientIdText.text withCompletionBlock:^(BOOL success, NSString *response, NSError *error) {
        if (response.length < 20 && response.length > 3 && !error) {
            [DetailsHelper registerUser:response];
            NSString *userId = [DetailsHelper userIdFromDevice];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:@"isLogin"];
            [self startApp:userId];
        } else {
            self.view.alpha = 1;
            self.loadingSpinner.hidden = YES;
            [self showAlertCode];
        }
    }];
}

- (void)showAlertCode {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה"
                                                    message:@"הקוד שהוזן אינו נמצא במערכת, אנא פנה לרופא"
                                                   delegate:nil
                                          cancelButtonTitle:@"אישור"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showAlertCanRun {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"אין באפשרותך לפתוח כעת את האפליקציה, פנה לרופא"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    
    NSString *userId = [DetailsHelper userIdFromDevice];
    if (userId != nil) {
        [self startApp:userId];
    } else {
        [self showScreen];
    }
}

- (void)showScreen {
    [self.textLabel1 setHidden:NO];
    self.textLabel2.text = @"שלום וברוך הבא לאפליקציה , נא הכנס קוד";
    [self.connectButton setHidden:NO];
    [self.patientIdText setHidden:NO];
    [self.loadingSpinner setHidden:YES];
    [self.loadingSpinner stopAnimating];
    self.view.alpha = 1;
}

- (void)initView {
    self.connectButton.layer.cornerRadius = 3;
    self.connectButton.layer.borderWidth = 1;
    self.connectButton.layer.borderColor = [UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1].CGColor;
    [self.textLabel1 setHidden:YES];
    self.textLabel2.text = @"אנא המתן";
    [self.connectButton setHidden:YES];
    [self.patientIdText setHidden:YES];
    [self.loadingSpinner setHidden:NO];
    [self.loadingSpinner startAnimating];
    self.view.alpha = 0.3;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Option"]) {
        OptionViewController *vc = [segue destinationViewController];
        vc.responseJson = _responseJson;
    } else if ([[segue identifier] isEqualToString:@"degOfPain"]) {
        OptionViewController *vc = [segue destinationViewController];
        vc.responseJson = _responseJson;
    } else if ([[segue identifier] isEqualToString:@"Recommendation"]) {
        RecommendationViewController *vc = [segue destinationViewController];
        vc.responseJson = _responseJson;
    }
}

- (void)startApp:(NSString *)userId {
    [DetailsHelper canRunApp:userId withCompletionBlock:^(BOOL success, NSString *response, NSError *error) {
        NSLog(@"[ViewController] received response for can run app:\n%@", response);
        BOOL canRun = NO;
        if (success) {
            canRun = [response boolValue];
        }
        if (canRun) {
            [PicardHelper StartNewSessionForJson:^(BOOL success, NSDictionary *responseJson, NSError *error) {
                NSLog(@"[ViewController] received response for start new session:\n%@", responseJson);
                self.responseJson = responseJson;
                if (success) {
                    NSString *segueStr = [SegueHelper nextScreen:_responseJson optionsScreen:@"Option" degOfPainScreen:@"degOfPain" reccomendationScreen:@"Recommendation"];
                    dispatch_async(dispatch_get_main_queue(),^{
                        [self.loadingSpinner stopAnimating];
                        [self performSegueWithIdentifier: segueStr sender: self];
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.loadingSpinner stopAnimating];
                    });
                    [PicardHelper showErrorAlert];
                }
            }];
        } else {
            [self.loadingSpinner stopAnimating];
            [self showAlertCanRun];
        }
    }];
}

@end
