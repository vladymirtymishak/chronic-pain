//
//  RecommendationViewController.m
//  ChronicPain
//
//  Created by noah alevi on 3/22/15.
//  Copyright (c) 2015 BGU. All rights reserved.
//

#import "RecommendationViewController.h"
#import "SegueHelper.h"
#import "Notifications.h"

@interface RecommendationViewController ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (nonatomic, copy) NSString *text;

@end

@implementation RecommendationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showRecommendation];

    if ([self showCloseButton]) {
        [self makeCloseButton];
    } else {
        [self makeContinueButton];
    }
}

- (void)setTimerAndwakeUpInHour {
    NSInteger sonId = [SegueHelper getSonResponseId:self.responseJson];
    NSArray *wakeUp = @[@17798, @20149, @17828, @20277, @20148];
    if ([wakeUp containsObject:@(sonId)]) {
        //lock app for hour
        NSTimeInterval reminderDelay = 60;
        [SegueHelper setTimer:reminderDelay];
        [Notifications scheduleReminderNotificationAfterMinutes:reminderDelay];
    }
}

- (BOOL)showCloseButton {
    NSInteger sonId= [SegueHelper getSonResponseId:self.responseJson];

    NSArray* closeButton = @[@17800, @20148, @20149, @20412, @20269, @20274, @20277, @20278, @17798, @17828, @17829];
    if([closeButton containsObject:@(sonId)]) {
        return YES;
    }
    return NO;
}

- (void)makeRecommendationLabel {
    self.text = [SegueHelper getMessageCotent:self.responseJson];
    NSInteger sonId = [SegueHelper getSonResponseId:self.responseJson];
    NSArray* asterisk = @[@20146, @20148, @20149, @20277, @20278, @17828, @17801];

    if ([asterisk containsObject:@(sonId)]) {
        NSString *message = [[SegueHelper getMessageCotent:self.responseJson] stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSString *parsed = @"";
        NSArray *lines = [message componentsSeparatedByString: @"*"];
        NSString *medicineName = lines[0];

        if (lines.count == 5) {
            if ([medicineName containsString:@"Oxycode"]) {
                NSInteger amount = [lines[3] integerValue];
                NSString *notes = [lines[4] stringByReplacingOccurrencesOfString:@"ml " withString:@""];

                parsed = [NSString stringWithFormat:@"טול\n%@\n%li סמ״ק, %@", medicineName, (long)amount, notes];
            }
            else if ([@[@"M.I.R", @"ABSTRAL", @"Percocet", @"Acamol", @"Ibuprofen", @"Optalgin"] containsObject:medicineName]) {
                NSInteger amount = [lines[1] integerValue];
                NSString *type = lines[2];
                NSInteger numTabs = [lines[3] integerValue];
                NSString *tabType = lines[4];
                NSString *numTabsString = [NSString stringWithFormat:numTabs > 1 ? @"%d כדורים" : @"כדור אחד", numTabs];

                parsed = [NSString stringWithFormat:@"טול\n%@ %d%@\n%@, %@", medicineName, amount, type, numTabsString, tabType];
                parsed = [parsed stringByReplacingOccurrencesOfString:@"Tabs " withString:@""];
            }
            else if ([medicineName isEqualToString:@"Actiq"]) {
                NSInteger amount = [lines[1] integerValue];
                NSString *type = lines[2];
                NSString *note = [lines[4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                parsed = [NSString stringWithFormat:@"טול\n%@ %d%@\n%@", medicineName, amount, type, note];
            }
            else if ([medicineName isEqualToString:@"Morphine HCL"]) {
                NSInteger amount = [lines[3] integerValue];
                NSString *numTabsString = [NSString stringWithFormat:@"%d מ״ג", amount];
                NSString *notes = [lines[4] stringByReplacingOccurrencesOfString:@"mg " withString:@""];

                parsed = [NSString stringWithFormat:@"טול\n%@\n%@, %@", medicineName, numTabsString, notes];
            }
            else if ([medicineName isEqualToString:@"PECFENT"]) {
                NSInteger amount = [lines[1] integerValue];
                NSString *type = lines[2];
                NSInteger numSprays = [lines[3] integerValue];
                NSString *numTabsString = [NSString stringWithFormat:numSprays > 1 ? @"%d מנות של" : @"מנה אחת של", numSprays];
                NSString *notes = [lines[4] stringByReplacingOccurrencesOfString:@"Sprays " withString:@""];

                parsed = [NSString stringWithFormat:@"נא לרסס בנחיר %@\n%@ %d%@\n%@", numTabsString, medicineName, amount, type, notes];
            }
        } else {
            parsed = lines[0];
        }

        BOOL shouldWaitForAnHour = [parsed containsString:@"המתן שעה"];
        if (shouldWaitForAnHour) {
            [self setTimerAndwakeUpInHour];
        }

        self.text = parsed;
    }
}

- (void)makeContinueButton {
    UIButton *oldButton = [self.view viewWithTag:321];
    if (oldButton) {
        [oldButton removeFromSuperview];
    }

    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(continueButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"המשך" forState:UIControlStateNormal];
    button.tintColor = [UIColor whiteColor];
    button.backgroundColor=[UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1];
    button.titleLabel.font = [UIFont systemFontOfSize:20];
    button.layer.cornerRadius = 3;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1].CGColor;
    [button setContentEdgeInsets:UIEdgeInsetsMake(10, 30, 10, 30)];
    button.tag = 321;
    
    [self.view addSubview:button];
    [button sizeToFit];

    button.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
}

- (void)makeCloseButton {
    UIButton *oldButton = [self.view viewWithTag:322];
    if (oldButton) {
        [oldButton removeFromSuperview];
    }

    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"סיום" forState:UIControlStateNormal];
    button.tintColor = [UIColor whiteColor];
    button.backgroundColor=[UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1];
    button.titleLabel.font = [UIFont systemFontOfSize:20];
    button.layer.cornerRadius = 3;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1].CGColor;
    [button setContentEdgeInsets:UIEdgeInsetsMake(10, 30, 10, 30)];
    button.tag = 322;
    
    [self.view addSubview:button];
    [button sizeToFit];

    button.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
}

- (IBAction)continueButtonClicked:(UIButton *)sender {
    [self.loadingSpinner startAnimating];
    NSMutableDictionary*responseMutable = [self.responseJson mutableCopy];

    NSMutableArray *messages = [responseMutable objectForKey:@"Messages"];
    NSDictionary *message = messages[0];
    NSMutableDictionary *m = [message mutableCopy];
    //put str of choise in result
    [m setObject:@"TRUE" forKey:@"ResultContent"];
    [messages replaceObjectAtIndex:0 withObject:m];
    
    self.view.alpha = 0.3;
    [self.loadingSpinner startAnimating];
    [PicardHelper GetRecommendations:responseMutable withCompletionBlock:^(BOOL success, NSDictionary *responseJson, NSError *error) {
        if (success) {
            NSLog(@"[RecommendationViewController] received response for get recommendations: %@", responseJson);
            self.responseJson = responseJson;
        } else {
            NSLog(@"[RecommendationViewController] error in recomendations response %@", error);
        }
        NSString* segueStr = [SegueHelper nextScreen:responseJson optionsScreen:@"RecommendationToOptions" degOfPainScreen:@"RecommendationToPain" reccomendationScreen:@"self"];
        dispatch_async(dispatch_get_main_queue(),^{
            self.view.alpha = 1;
            [self.loadingSpinner stopAnimating];
            if ([segueStr isEqualToString:@"self"]) {
                [self reloadView];
            } else {
                [self performSegueWithIdentifier: segueStr sender: self];
            }
        });
    }];
}

- (void)reloadView {
    [self viewDidLoad];
    [self.loadingSpinner stopAnimating];
}

- (IBAction)closeButtonClicked:(UIButton *)sender {
    [self.loadingSpinner startAnimating];
    NSMutableDictionary*responseMutable = [self.responseJson mutableCopy];

    NSMutableArray *messages = [responseMutable objectForKey:@"Messages"];
    NSDictionary* message = messages[0];
    NSMutableDictionary* m = [message mutableCopy];
    
    //put str of choise in result
    [m setObject:@"TRUE" forKey:@"ResultContent"];
    [messages replaceObjectAtIndex:0 withObject:m];
    
    self.view.alpha = 0.3;
    [self.loadingSpinner startAnimating];
    [PicardHelper GetRecommendations:responseMutable withCompletionBlock:^(BOOL success, NSDictionary *responseJson, NSError *error) {
        dispatch_async(dispatch_get_main_queue(),^{
            self.view.alpha = 1;
            [self.loadingSpinner stopAnimating];
            [self performSegueWithIdentifier: @"RecommendationToBlocking" sender: self];
        });
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"RecommendationToBlocking"]) {
        BlockingViewController *vc = [segue destinationViewController];
        vc.blockingMessage = @"היעוץ הנוכחי הסתיים, אנא צא מהאפליקציה";
    }
}

- (void)showRecommendation {
    //make the currect string in recommendation property
    [self makeRecommendationLabel];
    NSString *message = self.text;
    message = [@"\u200F" stringByAppendingString:message];

    self.textLabel.text = message;
}

@end
