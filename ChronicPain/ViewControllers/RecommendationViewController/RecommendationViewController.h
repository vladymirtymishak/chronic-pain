//
//  RecommendationViewController.h
//  ChronicPain
//
//  Created by noah alevi on 3/22/15.
//  Copyright (c) 2015 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailsHelper.h"
#import "PicardHelper.h"
#import "SegueHelper.h"
#import "BlockingViewController.h"

@interface RecommendationViewController : UIViewController

@property (nonatomic, strong) NSDictionary *responseJson;

@end
