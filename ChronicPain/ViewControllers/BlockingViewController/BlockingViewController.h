//
//  BlockingViewController.h
//  ChronicPain
//
//  Created by noah alevi on 5/10/15.
//  Copyright (c) 2015 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockingViewController : UIViewController

@property (nonatomic, copy) NSString *blockingMessage;

@end
