//
//  BlockingViewController.m
//  ChronicPain
//
//  Created by noah alevi on 5/10/15.
//  Copyright (c) 2015 BGU. All rights reserved.
//

#import "BlockingViewController.h"
#import "SegueHelper.h"

@interface BlockingViewController ()

@property (weak, nonatomic) IBOutlet UILabel *blockingMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;

@end

@implementation BlockingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTextLabel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onApplicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onApplicationDidEnterBackground:(NSNotification *)notification {
    exit(0);
}

- (IBAction)onExitButtonPressed:(UIButton *)sender {
    sender.enabled = NO;
    [NSThread sleepForTimeInterval:1.0];
    abort();
}

- (void)setupTextLabel {
    self.blockingMessageLabel.text = self.blockingMessage;
    self.blockingMessageLabel.backgroundColor = [UIColor colorWithRed:57/255.0 green:188/255.0 blue:243/255.0 alpha:1];
    self.blockingMessageLabel.textColor = [UIColor whiteColor];
}

@end
