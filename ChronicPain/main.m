//
//  main.m
//  ChronicPain
//
//  Created by noah alevi on 9/7/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
