//
//  AppDelegate.m
//  ChronicPain
//
//  Created by noah alevi on 9/7/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import "AppDelegate.h"
#import "Notifications.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Notifications registerApplication:application];
    [Notifications cancelAll];
    
    NSDate *reminderFireDate = [Notifications restoreReminderNotification];
    // No need to schedule daily notifications for the same period as reminder notification
    if (reminderFireDate != nil) {
        [Notifications scheduleSnoozableNotificationForDays:kFurtherNotificationsDaysCount startingFromDate:reminderFireDate withRemindersCount:kNotificationRemindersCount];
    } else {
        [Notifications scheduleSnoozableNotificationForDays:kFurtherNotificationsDaysCount startingFromDate:[NSDate date] withRemindersCount:kNotificationRemindersCount];
    }
    
    NSLog(@"Scheduled notifications: %@", [[UIApplication sharedApplication] scheduledLocalNotifications]);

    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    if ([self isBlockingTimerRunning]) {
        [self showEndSessionController];
    }
}

#pragma mark - Notifications

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    [self showInitialController];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    completionHandler();
}

#pragma mark - Private

- (BOOL)isBlockingTimerRunning {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *appUnlockDate = [defaults objectForKey:@"timer"];
    if ([appUnlockDate isKindOfClass:[NSDate class]] == NO) {
        return NO;
    }
    NSLog(@"\n\nThe time now is: %@,\nApp is locked till: %@\n", [NSDate date], appUnlockDate);
    
    BOOL isRunning = ([[NSDate date] compare:appUnlockDate] == NSOrderedAscending);
    return isRunning;
}

- (void)clearTimer {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"timer"];
    [defaults synchronize];
}

- (void)showEndSessionController {
    BlockingViewController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"endSession"];
    controller.blockingMessage = @"אינך יכול להכנס לאפליקציה כרגע.נא המתן עד תום השעה.";
    self.window.rootViewController = controller;
}

- (void)showInitialController {
    self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"initial"];
}

@end
