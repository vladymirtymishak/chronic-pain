//
//  AppDelegate.h
//  ChronicPain
//
//  Created by noah alevi on 9/7/14.
//  Copyright (c) 2014 BGU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SegueHelper.h"
#import "BlockingViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
